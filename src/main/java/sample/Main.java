package sample;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

public class Main {

    public static void main(String[] args) {
        String url = "http://localhost:8080/parking/";

        Credential credential = new Credential();
        credential.setUsername("username");
        credential.setPassword("password");

        ResteasyClient client = (ResteasyClient) ClientBuilder.newClient();
        ResteasyWebTarget target = client.target(url);

        AutenticationService service = target.proxy(AutenticationService.class);
        Response response = service.login(credential);

        System.out.println("CODE: " + response.getStatus());
        System.out.println("BODY: " + response.readEntity(String.class));
    }
}
